package net.threetag.pymtech.ability;

public interface IAdvancedRegulatorAbility {

    void setRegulatedSize(float size);

    float getRegulatedSize();

}
